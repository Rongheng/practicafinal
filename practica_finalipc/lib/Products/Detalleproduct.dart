import 'package:flutter/material.dart';
import 'Modeloproduct.dart';
import 'Cartlist.dart';

class Detalle extends StatelessWidget {
  final Product product;
  final Cartlist cart;

  const Detalle({Key? key, required this.product, required this.cart})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalles del Producto'),
      ),
      backgroundColor: const Color.fromARGB(255, 253, 241, 255),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
              tag: product.imageUrl,
              child: Center(
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    product.imageUrl,
                    width: 350,
                    height: 300,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 227, 247, 250),
                border:
                    Border.all(color: const Color.fromARGB(255, 226, 96, 96)),
              ),
              child: Column(
                children: [
                  Text(
                    product.name,
                    style: const TextStyle(
                        fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    '\$${product.price.toStringAsFixed(2)}',
                    style: const TextStyle(fontSize: 18),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    product.description,
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () {
                      cart.addProduct(product);
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                            content: Text('Producto añadido al carrito')),
                      );
                    },
                    child: const Text('Añadir al Carrito'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
