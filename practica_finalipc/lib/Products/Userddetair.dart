import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserDetailsPage extends StatelessWidget {
  const UserDetailsPage({Key? key}) : super(key: key);

  Future<Map<String, dynamic>> _getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? username = prefs.getString('username');
    String? email = prefs.getString('email');
    String? password = prefs.getString('password');
    return {'username': username, 'email': email, 'password': password};
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalles del Usuario'),
        backgroundColor: Colors.purple,
      ),
      body: FutureBuilder<Map<String, dynamic>>(
        future: _getUserDetails(),
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError ||
              !snapshot.hasData ||
              snapshot.data!['username'] == null) {
            return const Center(
                child: Text('Error al cargar los datos del usuario.'));
          } else {
            String username = snapshot.data!['username']!;
            String email = snapshot.data!['email']!;
            String password = snapshot.data!['password'] ?? 'No disponible';

            return Center(
              child: Container(
                padding: const EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 10,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage('assets/profile_image.png'),
                      child: FlutterLogo(size: 50),
                    ),
                    const SizedBox(height: 10),
                    Text('Nombre de usuario: $username',
                        style: const TextStyle(fontSize: 18)),
                    const SizedBox(height: 10),
                    Text('Correo electrónico: $email',
                        style: const TextStyle(fontSize: 18)),
                    const SizedBox(height: 10),
                    Text('Contraseña: $password',
                        style: const TextStyle(fontSize: 18)),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
