import 'package:flutter/material.dart';
import 'package:practica_finalipc/Products/Cartlist.dart';
import 'dart:convert';
import 'Modeloproduct.dart';
import 'Detalleproduct.dart';
import 'ConfirmarP.dart';
import 'Userddetair.dart';

class Productospage extends StatelessWidget {
  const Productospage({Key? key}) : super(key: key);

  Future<String> cargarArchivo(BuildContext context) async {
    return await DefaultAssetBundle.of(context)
        .loadString('asset/json/products.json');
  }

  @override
  Widget build(BuildContext context) {
    final Cartlist cart = Cartlist();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Productos'),
        titleTextStyle: const TextStyle(color: Colors.white, fontSize: 20),
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const UserDetailsPage(),
                ),
              );
            },
            icon: const Icon(Icons.person),
          ),
          Transform.rotate(
            angle: 330 * 3.1415926535 / 180,
            child: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ConfirmarPago(cart: cart),
                  ),
                );
              },
              icon: const Icon(Icons.shopping_cart),
            ),
          ),
        ],
      ),
      body: Container(
        child: Center(
          child: FutureBuilder(
            future: cargarArchivo(context),
            builder: (context, AsyncSnapshot<String> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              } else if (snapshot.hasError) {
                return const Center(child: Text('Error al cargar los datos'));
              } else if (!snapshot.hasData) {
                return const Center(child: Text('No hay datos disponibles'));
              } else {
                Map<String, dynamic> data = json.decode(snapshot.data!);
                var products = Products.fromJson(data);
                return ListView.builder(
                  itemCount: products.products.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Detalle(
                              product: products.products[index],
                              cart: cart,
                            ),
                          ),
                        );
                      },
                      child: Hero(
                        tag: "product_${products.products[index].name}",
                        child: Card(
                          child: ListTile(
                            title: Text(products.products[index].name),
                            subtitle: Text("${products.products[index].price}"),
                            leading: Image.asset(
                              products.products[index].imageUrl,
                              width: 40,
                              height: 40,
                            ),
                            trailing: const Icon(Icons.arrow_forward),
                          ),
                        ),
                      ),
                    );
                  },
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
