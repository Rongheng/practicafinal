import 'Modeloproduct.dart';

class Cartlist {
  final List<Product> _products = [];

  List<Product> get products => _products;

  void addProduct(Product product) {
    _products.add(product);
  }

  void removeProduct(Product product) {
    _products.remove(product);
  }

  void clearCart() {
    _products.clear();
  }

  double get totalPrice {
    return _products.fold(0.0, (sum, item) => sum + item.price);
  }

  double calculateTotalAmount() {
    double total = 0;
    for (var product in _products) {
      total += product.price;
    }
    return total;
  }
}
