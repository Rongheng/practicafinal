import 'package:flutter/material.dart';
import 'Cartlist.dart';
// ignore: unused_import
import 'Modeloproduct.dart';
import 'ConfirmarP.dart';

class CarritoPage extends StatelessWidget {
  final Cartlist cart;

  const CarritoPage({Key? key, required this.cart}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Carrito de Compras'),
        backgroundColor: Colors.purple,
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: cart.products.length,
              itemBuilder: (context, index) {
                final product = cart.products[index];
                return ListTile(
                  leading: Image.asset(
                    product.imageUrl,
                    width: 50,
                    height: 50,
                  ),
                  title: Text(product.name),
                  subtitle: Text('\$${product.price.toStringAsFixed(2)}'),
                  trailing: IconButton(
                    icon: const Icon(Icons.remove_circle),
                    onPressed: () {
                      cart.removeProduct(product);
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: const Text('Producto eliminado del carrito'),
                          action: SnackBarAction(
                            label: 'Deshacer',
                            onPressed: () {
                              cart.addProduct(product);
                            },
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ConfirmarPago(cart: cart),
                ),
              );
            },
            child: const Text('Confirmar Pago'),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
