import 'package:flutter/material.dart';
import 'Cartlist.dart';

class ConfirmarPago extends StatefulWidget {
  final Cartlist cart;

  const ConfirmarPago({Key? key, required this.cart}) : super(key: key);

  @override
  _ConfirmarPagoState createState() => _ConfirmarPagoState();
}

class _ConfirmarPagoState extends State<ConfirmarPago> {
  bool _contrarembolso = false;
  bool _mastercard = false;
  bool _venderElAlma = false;
  final TextEditingController _addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double totalAmount = widget.cart.calculateTotalAmount();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Confirmar Pago'),
        backgroundColor: Colors.purple,
      ),
      body: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    'Resumen del Carrito:',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: widget.cart.products.length,
                    itemBuilder: (context, index) {
                      final product = widget.cart.products[index];
                      return Dismissible(
                        key: Key(product.name),
                        direction: DismissDirection.endToStart,
                        onDismissed: (direction) {
                          widget.cart.removeProduct(product);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content:
                                  Text('${product.name} eliminado del carrito'),
                              action: SnackBarAction(
                                label: 'Deshacer',
                                onPressed: () {
                                  widget.cart.addProduct(product);
                                },
                              ),
                            ),
                          );
                        },
                        background: Container(
                          color: Colors.red,
                          alignment: Alignment.centerRight,
                          child: const Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                        ),
                        child: ListTile(
                          leading: Image.asset(
                            product.imageUrl,
                            width: 50,
                            height: 50,
                          ),
                          title: Text(product.name),
                          subtitle:
                              Text('\$${product.price.toStringAsFixed(2)}'),
                        ),
                      );
                    },
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        final TextEditingController _dialogAddressController =
                            TextEditingController();
                        return AlertDialog(
                          title: const Text('Confirmar Pago'),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Total a pagar: \$${totalAmount.toStringAsFixed(2)}'),
                              const SizedBox(height: 10),
                              TextField(
                                controller: _dialogAddressController,
                                decoration: const InputDecoration(
                                  labelText: 'Dirección de Envío',
                                ),
                              ),
                              const SizedBox(height: 20),
                              ElevatedButton(
                                onPressed: () {
                                  if (_dialogAddressController.text ==
                                      _addressController.text) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Pago confirmado'),
                                      ),
                                    );
                                    widget.cart.clearCart();
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text('La dirección no coincide'),
                                      ),
                                    );
                                  }
                                },
                                child: const Text('Confirmar Pago'),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  child: const Text('Confirmar Pago'),
                ),
                const SizedBox(height: 20),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancelar'),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.purple),
                borderRadius: BorderRadius.circular(12),
              ),
              padding: const EdgeInsets.all(16.0),
              margin: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Text(
                    'Opciones Adicionales:',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  Expanded(
                    child: ListView(
                      children: [
                        CheckboxListTile(
                          secondary: Image.asset('asset/images/rembolso.png',
                              width: 50, height: 50),
                          title: const Text('contrarembolso'),
                          value: _contrarembolso,
                          onChanged: (bool? value) {
                            setState(() {
                              _contrarembolso = value ?? false;
                            });
                          },
                        ),
                        CheckboxListTile(
                          secondary: Image.asset('asset/images/card.png',
                              width: 50, height: 50),
                          title: const Text('Mastercard'),
                          value: _mastercard,
                          onChanged: (bool? value) {
                            setState(() {
                              _mastercard = value ?? false;
                            });
                          },
                        ),
                        CheckboxListTile(
                          secondary: Image.asset('asset/images/alma.png',
                              width: 50, height: 50),
                          title: const Text('vender el alma'),
                          value: _venderElAlma,
                          onChanged: (bool? value) {
                            setState(() {
                              _venderElAlma = value ?? false;
                            });
                          },
                        ),
                        const SizedBox(height: 20),
                        TextField(
                          controller: _addressController,
                          decoration: const InputDecoration(
                            labelText: 'Dirección de Envío',
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
